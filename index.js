// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть
// реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить.
// Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.

let userNumber1 = +prompt('Enter first number.');
let userNumber2 = +prompt('Enter second number.');
let userOperation = prompt('Enter math operation here (+, -, *, /).');

function calculator(userNumber1, userNumber2, userOperation) {
    switch (userOperation) {
        case '+':
          return userNumber1 + userNumber2;
        case '-':
          return userNumber1 - userNumber2;
        case '*':
          return userNumber1 * userNumber2;
        case '/':
          return userNumber1 / userNumber2;
}
}
console.log(calculator(userNumber1, userNumber2, userOperation));



